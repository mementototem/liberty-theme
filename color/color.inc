<?php

$info = array(

  // Available colors and color labels used in theme.
  'fields' => array(
    'base' => t('Base color'),
    'link' => t('Link color'),
    'text' => t('Text color'),
  ),
  // Pre-defined color schemes.
  'schemes' => array(
    'default' => array(
      'title' => t('Aquamarine (Default)'),
      'colors' => array(
        'base' => '#55c0e2',
        'link' => '#027ac6',
        'text' => '#494949',
      ),
    ),
    'diamond' => array(
      'title' => t('Diamond'),
      'colors' => array(
        'base' => '#dbdbdb',
        'link' => '#000000',
        'text' => '#646464',
      ),
    ),
    'ruby' => array(
      'title' => t('Ruby'),
      'colors' => array(
        'base' => '#ff3c2d',
        'link' => '#ab0b07',
        'text' => '#333333',
      ),
    ),
    'emerald' => array(
      'title' => t('Emerald'),
      'colors' => array(
        'base' => '#219310',
        'link' => '#1b6a12',
        'text' => '#494949',
      ),
    ),
    'yellowsapphire' => array(
      'title' => t('Yellow Sapphire'),
      'colors' => array(
        'base' => '#ffcf19',
        'link' => '#d39812',
        'text' => '#474747',
      ),
    ),
    'garnet' => array(
      'title' => t('Garnet'),
      'colors' => array(
        'base' => '#c52111',
        'link' => '#b71b05',
        'text' => '#333333',
      ),
    ),
    'sapphire' => array(
      'title' => t('Sapphire'),
      'colors' => array(
        'base' => '#3d10df',
        'link' => '#2315ad',
        'text' => '#494949',
      ),
    ),
    'moonstone' => array(
      'title' => t('Moon Stone'),
      'colors' => array(
        'base' => '#707070',
        'link' => '#06097f',
        'text' => '#6b6b6b',
      ),
    ),
    'zircon' => array(
      'title' => t('Zircon'),
      'colors' => array(
        'base' => '#c14700',
        'link' => '#641f11',
        'text' => '#494949',
      ),
    ),
    'cateye' => array(
      'title' => t('Chrysoberyl-cat eye'),
      'colors' => array(
        'base' => '#313d77',
        'link' => '#0419ae',
        'text' => '#494949',
      ),
    ),
    'amethyst' => array(
      'title' => t('Amethyst'),
      'colors' => array(
        'base' => '#6d07d9',
        'link' => '#624096',
        'text' => '#494949',
      ),
    ),
  ),

  // Images to copy over.
  'copy' => array(
    'images/facebook.png',
    'images/rss.png',
    'images/twitter.png',
    
    'images/menu-collapsed.gif',
    'images/menu-collapsed-rtl.gif',
    'images/menu-expanded.gif',
    'images/menu-leaf.gif',
    
    'logo.png',
  ),

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'css/colors.css',
    'css/colors-rtl.css',
  ),

  'gradients' => array(
    array(
      // (x, y, width, height).
      'dimension' => array(550, 0, 50, 50),
      // Direction of gradient ('vertical' or 'horizontal').
      'direction' => 'vertical',
      // Keys of colors to use for the gradient.
      'colors' => array('base', 'base'),
    ),
  ),

  // Color areas to fill (x, y, width, height).
  'fill' => array(
    'base' => array(0, 0, 600, 300),
  ),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(
    'images/bg-header.png'        => array(0, 140, 600, 160),
    'images/bg-menu-sub.png'      => array(175, 24, 10, 18),
  ),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files.
  'preview_image' => 'color/preview.png',
  'preview_css' => 'color/preview.css',

  // Base file for image generation.
  'base_image' => 'color/base.png',
);
