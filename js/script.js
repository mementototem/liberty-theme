jQuery(function($) {
	// Fix content height
	var sidebar_height = $("#sidebar-main").height();
	var content_height = $("#main").height();
	
	if ((sidebar_height > content_height) && ('none' != $("#sidebar-main").css('display'))) {
		$("#main").css('min-height', sidebar_height);
	}
});