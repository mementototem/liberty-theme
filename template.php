<?php
// $Id $

/**
 * Add body classes if certain regions have content.
 */
function liberty_preprocess_html(&$variables) {
  if (!empty($variables['page']['featured'])) {
    $variables['classes_array'][] = 'featured';
  }

  if (!empty($variables['page']['footer_firstcolumn'])
    || !empty($variables['page']['footer_secondcolumn'])
    || !empty($variables['page']['footer_thirdcolumn'])) {
    $variables['classes_array'][] = 'footer-columns';
  }

  // Add conditional stylesheets for IE
  drupal_add_css(path_to_theme() . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
  drupal_add_css(path_to_theme() . '/css/ie6.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 6', '!IE' => FALSE), 'preprocess' => FALSE));
  
  // External web fonts
  drupal_add_css('http://fonts.googleapis.com/css?family=Berkshire+Swash', array('group' => CSS_THEME, 'preprocess' => FALSE));
  drupal_add_css(path_to_theme() . '/css/fonts/th_k2d_july8.css', array('group' => CSS_THEME, 'preprocess' => FALSE));
  
  // Custom JavaScript
  drupal_add_js(path_to_theme() . '/js/script.js');
  
  // Add social sharing javascripts
  if (theme_get_setting('liberty_share_plusone') && empty($variables['is_front'])) {
    drupal_add_js('http://apis.google.com/js/plusone.js', 'external');
  }
  if (theme_get_setting('liberty_share_twitter') && empty($variables['is_front'])) {
    drupal_add_js('http://platform.twitter.com/widgets.js', 'external');
  }
  if (theme_get_setting('liberty_share_facebook') && empty($variables['is_front'])) {
    $og_type = array(
      '#type'       => 'html_tag',
      '#tag'        => 'meta',
      '#attributes' => array(
                        'property'  => 'og:type',
                        'content'   => check_plain(theme_get_setting('liberty_og_type')),
                       ),
    );
    $og_sitename = array(
      '#type'       => 'html_tag',
      '#tag'        => 'meta',
      '#attributes' => array(
                        'property'  => 'og:site_name',
                        'content'   => filter_xss_admin(variable_get('site_name', 'Drupal')),
                       ),
    );
    $og_admin = array(
      '#type'       => 'html_tag',
      '#tag'        => 'meta',
      '#attributes' => array(
                        'property'  => 'fb:admins',
                        'content'   => check_plain(theme_get_setting('liberty_og_admin')),
                       ),
    );
    drupal_add_html_head($og_type, 'og_type');
    drupal_add_html_head($og_sitename, 'og_sitename');
    drupal_add_html_head($og_admin, 'og_admin');
  }
}

/**
 * Override or insert variables into the page template for HTML output.
 */
function liberty_process_html(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_html_alter($variables);
  }
}

/**
 * Override or insert variables into the page template.
 */
function liberty_process_page(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_page_alter($variables);
  }
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
  // Since the title and the shortcut link are both block level elements,
  // positioning them next to each other is much simpler with a wrapper div.
  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
    // Add a wrapper div using the title_prefix and title_suffix render elements.
    $variables['title_prefix']['shortcut_wrapper'] = array(
      '#markup' => '<div class="shortcut-wrapper clearfix">',
      '#weight' => 100,
    );
    $variables['title_suffix']['shortcut_wrapper'] = array(
      '#markup' => '</div>',
      '#weight' => -99,
    );
    // Make sure the shortcut link is the first item in title_suffix.
    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
  }
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function liberty_preprocess_maintenance_page(&$variables) {
  if (!$variables['db_is_active']) {
    unset($variables['site_name']);
  }
  drupal_add_css(drupal_get_path('theme', 'liberty') . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function liberty_process_maintenance_page(&$variables) {
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
}

/**
 * Override or insert variables into the node template.
 */
function liberty_preprocess_node(&$variables) {
  $variables['submitted'] = t('By !username On !datetime', array('!username' => $variables['name'], '!datetime' => $variables['date']));
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
  $variables['liberty_button_plusone'] = '';
  if (theme_get_setting('liberty_share_plusone')) {
    $variables['liberty_button_plusone'] = '<g:plusone size="medium"></g:plusone><br />';
  }
  $variables['liberty_button_tweet'] = '';
  if (theme_get_setting('liberty_share_twitter')) {
    $variables['liberty_button_tweet'] = '<a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal">Tweet</a><br />';
  }
  $variables['liberty_button_like'] = '';
  if (theme_get_setting('liberty_share_facebook')) {
    $variables['liberty_button_like'] = '<div id="fb-root" style="display: inline"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="" send="true" width="900px" show_faces="false" action="recommend" font="lucida grande"></fb:like><br />';
  }
}

/**
 * Override or insert variables into the block template.
 */
function liberty_preprocess_block(&$variables) {
  // In the header region visually hide block titles.
  if ($variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }
}

/**
 * Implements theme_menu_tree().
 */
function liberty_menu_tree($variables) {
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_field__field_type().
 */
function liberty_field__taxonomy_term_reference($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
  }

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '">' . $output . '</div>';

  return $output;
}

/**
 * Change feed icon
 */
function liberty_feed_icon($variables) {
	$text = t('Subscribe to @feed-title', array('@feed-title' => $variables['title']));
	$link = t('RSS');
	if (theme_get_setting('liberty_socialbar_images')) {
	  $image = theme('image', array(
	    'path'    => path_to_theme() . '/images/rss.png',
	    'width'   => 32,
	    'height'  => 32,
	    'alt'     => $text,
	  ));
	  if ($image) {
	    $link = $image;
	  }
	}
	return l($link, $variables['url'], array('html' => TRUE, 'attributes' => array('class' => array('feed-icon'), 'title' => $text)));
}

function liberty_menu_display($menu, $option) {
  if (!isset($menu)) {
    return false;
  }
  
  $html  = '<' . $option['heading']['level'] . ' class="' . implode(' ', $option['heading']['class']) . '">';
  $html .= $option['heading']['text'];
  $html .= '</' . $option['heading']['level'] . '>';
  
  $html .= '<ul id="' . $option['attributes']['id'] . '" class="' . implode(' ', $option['attributes']['class']) . '">';
  
  $count = 1;
  $alls = count($menu);
  
  foreach($menu as $key => $value) {
    $html .= '<li class="' . $key;
    
    if (1 == $count)
      $html .= ' first';
    elseif ($alls == $count)
      $html .= ' last';
    
    $html .= '">';
    $html .= '<a href="' . url($value['href'], array('absolute' => true)) . '">';
    $html .= '<span class="menu-name">' . $value['title'] . '</span>';
    $html .= '<span class="menu-description">' . $value['attributes']['title'] . '</span>';
    $html .= '</a>';
    $html .= "</li>\n";
    
    $count++;
  }
  
  $html .= '</ul>';
  
  return $html;
}