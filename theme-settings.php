<?php
/**
 * $Id$
 *
 * Implementation of THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   array An array of saved settings for this theme.
 * @return
 *   array A form array.
**/

function liberty_form_system_theme_settings_alter(&$form, $form_state) {
	// Create the form widgets using Forms API

	$form['liberty_share_twitter'] = array(
	  '#type'           => 'checkbox',
	  '#title'          => t('Display Tweet button'),
	  '#description'    => t('Add basic Tweet button on page. If you want more customizable use <a href="!link">Tweet Button module</a> instead.', array('!link' => 'http://drupal.org/project/tweetbutton')),
	  '#default_value'  => theme_get_setting('liberty_share_twitter'),
	);
	

	$form['liberty_share_plusone'] = array(
	  '#type'           => 'checkbox',
	  '#title'          => t('Display Google +1 button'),
	  '#description'    => t('Add basic Google +1 button on page.'),
	  '#default_value'  => theme_get_setting('liberty_share_plusone'),
	);
	
	$form['liberty_share_facebook'] = array(
	  '#type'           => 'checkbox',
	  '#title'          => t('Display Facebook Like and Send button'),
	  '#description'    => t('Add basic Facebook Like and Facebook Send button on page. If you want more customizable use <a href="!link">Facebook Share module</a> instead.', array('!link' => 'http://drupal.org/project/facebookshare')),
	  '#default_value'  => theme_get_setting('liberty_share_facebook'),
	);

	$form['liberty_og_type'] = array(
	  '#type'           => 'select',
	  '#title'          => t('Open Graph: Type of your node'),
	  '#description'    => t('Type of your node, for more information read <a href="!link">Open Graph protocal</a>.', array('!link' => 'https://developers.facebook.com/docs/reference/plugins/like/')),
	  '#default_value'  => theme_get_setting('liberty_og_type'),
	  '#options'        => array(
	                        'activity'      => 'activity',
	                        'sport'         => 'sport',
	                        'bar'           => 'bar',
	                        'company'       => 'company',
	                        'cafe'          => 'cafe',
	                        'hotel'         => 'hotel',
	                        'restaurant'    => 'restaurant',
	                        'cause'         => 'cause',
	                        'sports_league' => 'sports league',
	                        'sports_team'   => 'sports team',
	                        'band'          => 'band',
	                        'government'    => 'government',
	                        'non_profit'    => 'non profit',
	                        'school'        => 'school',
	                        'university'    => 'university',
	                        'actor'         => 'actor',
	                        'athlete'       => 'athlete',
	                        'author'        => 'author',
	                        'director'      => 'director',
	                        'musician'      => 'musician',
	                        'politician'    => 'politician',
	                        'public_figure' => 'public figure',
	                        'city'          => 'city',
	                        'country'       => 'country',
	                        'landmark'      => 'landmark',
	                        'state_province'=> 'state province',
	                        'album'         => 'album',
	                        'book'          => 'book',
	                        'drink'         => 'drink',
	                        'food'          => 'food',
	                        'game'          => 'game',
	                        'product'       => 'product',
	                        'song'          => 'song',
	                        'movie'         => 'movie',
	                        'tv_show'       => 'tv show',
	                        'blog'          => 'blog',
	                        'website'       => 'website',
	                        'article'       => 'article',
	                       ),
	);
	
	$form['liberty_og_admin'] = array(
	  '#type'           => 'textfield',
	  '#title'          => t('Open Graph: Admin IDs'),
	  '#description'    => t('A comma-separated list of either the Facebook IDs of page administrators or a Facebook Platform application ID. You can get this by looking in Admin box in <a href="!link">Facebook\'s plugins page</a>.', array('!link' => 'https://developers.facebook.com/docs/reference/plugins/like/')),
	  '#default_value'  => theme_get_setting('liberty_og_admin'),
	);

	$form['liberty_comment_title'] = array(
		'#type'           => 'checkbox',
		'#title'          => t('Display comment title'),
		'#description'    => t('This setting only hide comment title. If you want to hide comment subject in input form, change the setting in Content Type instead.'),
		'#default_value'	=> theme_get_setting('liberty_comment_title'),
	);

}

?>
